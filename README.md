# devopscamp-prometheus

The repository houses the configuration for our Prometheus installation.

Gitlab CI deploys the Helm chart using the template_values.yaml contained herein.

To see which values have been customized
- Grab the source
  `curl https://raw.githubusercontent.com/helm/charts/master/stable/prometheus/values.yaml > /tmp/values.yaml`
- Compare just the changes
  - `diff --side-by-side --suppress-common-lines /tmp/values.yaml template_values.yaml`
- Compare with more context
  - `diff --left-column --side-by-side /tmp/values.yaml template_values.yaml | less`

---

#### Happy Path

This repository is intended to run via Gitlab CI.  It expects the following variables will be available at build time:

- `PROMETHEUS_ADMIN_PASSWORD`
  - This should usually be stored as `DEV_PROMETHEUS_ADMIN_PASSWORD` and `PROD_PROMETHEUS_ADMIN_PASSWORD`.
  - Because these are more specific to the repository than the organization, they can be stored in the repo rather than the group.
  - These will generally be translated at deploy time.  See `.gitlab-ci.yml` for more information.

- Ritual de lo Habitual:
  - As usual, these should be inherited from the group level
  - `GCP_AUTH_KEYFILE`
  - `CLUSTER`
  - `REGION`
  - `PROJECT`
  - `DOMAIN`

---

#### Manual installation

- Create a secrets file for Prometheus login
  - `echo ${PROMETHEUS_ADMIN_PASSWORD} | htpasswd -i -c auth admin`
  - `kubectl create secret generic prometheus-server-auth --from-file=auth --dry-run -o yaml > parsed_secret.yaml`
- Apply the secret on your cluster
  - `kubectl apply -f parsed_secret.yaml`
- Parse your domain into the template
  - `sed -e "s/DOMAIN/${MACGUFFIN_DOMAIN}/" template_values.yaml > parsed_values.yaml`
- Install Prometheus using your local values
  - `helm install --tiller-namespace tiller --name ${MACGUFFIN}-prometheus --values parsed_values.yaml stable/prometheus`
- Or upgrade an existing installation
  - `helm upgrade --tiller-namespace tiller --values parsed_values.yaml ${MACGUFFIN}-prometheus stable/prometheus`

---

#### Examining the Template

Run these commands in the root of this repository.

```
mkdir -p ~/git/tmp
git clone git@github.com:helm/charts.git ~/git/tmp/charts
helm template --name exampletest --values values.yaml ~/git/tmp/charts/stable/prometheus
```
